# Contributing to the documentation of the Model Data Explorer

First of all, thanks! :tada: We are always happy about any kind of
contributions to help us improving the model data explorer and it's
documentation.

The source Model Data Explorer is publicly available at the Gitlab of the
_Helmholtz-Zentrum Dresden Rossendorf_, HZDR. You can sign up at
https://gitlab.hzdr.de with your Github account or through the Helmholtz AAI
(if you are employed at a member insitution of the HGF).

## Report issues, give feedback or ask for support

If you encounter any issues or have questions for support, please create an
issue in the main repository of this documentation, here: https://gitlab.hzdr.de/model-data-explorer/prototype/-/issues

If you want to reach the core development team directly, please do not hesitate
to send a mail to hcdc_support@hereon.de

## Contribute to the documentation

If you want to build and modify the documentation yourself, please have a look
into the _Installation_ section in the [README](README.md) of this repository.

### Get your name listed

We are very thankful for contributions during the prototype development and
do our best to acknowledge your efforts. For this purpose, please add your name
to [source/authors.yml](source/authors.yml).

Please add your name and, ideally, your OrcID. Both will be shown online.

Each person that is listed in this document will be included as a co-author in
the EPub and PDF versions of this document, and he or she will be listed on the
frontpage of the prototype development under *Contributors*.

The rules for sorting the authors are:

1. members from the development team (currently only HCDC)
2. other contributors sorted by first name

This document is also used for generating a list of section authors in the
documentation. Each person that participated in a user story of one of the
[epics][epics], will be listed on the corresponding page (see
[here][relations-story] for example).

[epics]: https://model-data-explorer.readthedocs.io/projects/prototype/en/latest/epics/index.html
[relations-story]: https://model-data-explorer.readthedocs.io/projects/prototype/en/latest/epics/metadata/relations.html