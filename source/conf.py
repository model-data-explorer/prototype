# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use osp.abspath to make it absolute, like shown here.
#
import os
import yaml
from docutils import nodes
from docutils.parsers.rst import directives
from sphinx_design.badges_buttons import LinkBadgeRole
from sphinx.directives import SphinxDirective
import urllib

# sys.path.insert(0, osp.abspath('.'))


# -- Project information -----------------------------------------------------

with open("authors.yml") as f:
    all_authors = yaml.safe_load(f)

project = "Prototype development of the Model Data Explorer"
copyright = "2021, Helmholtz-Zentrum hereon GmbH"
author = ", ".join(all_authors)

version = "0.0.1"

# The full version, including alpha/beta/rc tags
release = "0.0.1dev0"

show_authors = True


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinx.ext.intersphinx",
    "sphinx.ext.graphviz",
    "sphinx.ext.todo",
    "sphinx_design",
    "mde_sphinx_ext",
]

master_doc = "index"

sd_fontawesome_latex = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ["_templates"]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "sphinx_rtd_theme"

html_logo = "_static/hereon_Logo.png"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ["_static"]

html_css_files = [
    "https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free@5/css/all.min.css"
]

html_theme_options = {
    "canonical_url": "https://model-data-explorer.pages.hzdr.de/prototype",
    "style_nav_header_background": "white",
    "collapse_navigation": False,
}

intersphinx_mapping = {
    "main": ("https://model-data-explorer.readthedocs.io/en/latest/", None),
}

todo_include_todos = True


gitlab_link = "https://gitlab.hzdr.de/model-data-explorer/prototype/"

rst_epilog = f"""
.. |figma_prototype| replace:: {os.getenv("FIGMA_PROTOTYPE")}
.. |orcid| image:: /_static/orcid.*
    :width: 16px
"""

latex_documents = [
    (
        master_doc,
        "mde-prototype.tex",
        "Model Data Explorer Prototype Development",
        r" \and ".join(all_authors),
        "manual",
    ),
]

epub_author = " & ".join(all_authors)

epub_show_urls = "no"

epub_description = """
This document describes the first phase of the development of the model data
explorer. Within the prototype development, we discuss the workflows and
develop a prototype to get a feeling how the model data explorer will work.
""".strip()

graphviz_dot_args = ["-Gdpi=120.0"]


def link_to_prototype(
    name, rawtext, text, lineno, inliner, options={}, content=[]
):
    if os.getenv("FIGMA_PROTOTYPE"):
        node = nodes.reference(
            rawtext,
            text,
            refuri=os.getenv("FIGMA_PROTOTYPE"),
            options=options,
        )
        return [node], []
    return [], []


class LinkToGitlabIssue(LinkBadgeRole):
    """A badge linking to a github issue."""

    def run(self):
        self.title = "#" + self.title
        self.target = gitlab_link + "-/issues/" + self.target
        return super().run()


def link_to_gitlab(
    name, rawtext, text, lineno, inliner, options={}, content=[]
):
    node = nodes.reference(
        rawtext,
        text,
        refuri=gitlab_link,
        options=options,
    )
    return [node], []


class LinkToGitlabIssues(LinkBadgeRole):
    """A badge linking to a github issue."""

    def run(self):
        self.title = "Browse issues"
        self.target = (
            gitlab_link
            + "-/issues/?label_name[]="
            + urllib.parse.quote(self.target)
        )
        return super().run()


class AuthorList(SphinxDirective):
    """A list of authors."""

    has_content = True

    option_spec = {"all": directives.flag}

    def run(self):
        """Display a list of authors"""
        para: nodes.Element = nodes.paragraph(translatable=False)
        emph = nodes.emphasis()
        para += emph

        all_messages = []
        authors = list(map(str, self.content))
        if "all" in self.options:
            authors.extend(a for a in all_authors if a not in authors)

        for i, author in enumerate(authors, 1):
            inodes, messages = self.state.inline_text(author, self.lineno)
            emph += inodes
            all_messages += messages
            if author in all_authors:
                author_props = all_authors[author]
                if author_props and "orcid" in author_props:
                    inodes, messages = self.state.inline_text(
                        " |orcid| ", self.lineno
                    )
                    target = "https://orcid.org/" + author_props["orcid"]
                    reference_node = nodes.reference(refuri=target)
                    reference_node += inodes
                    emph += reference_node
                    all_messages += messages
            if i < len(authors):
                emph += self.state.inline_text(", ", self.lineno)[0]

        return [para] + all_messages


def setup(app):
    """Add the role to link to gitlab issues."""
    app.add_directive("authorlist", AuthorList)

    app.add_role("prototype", link_to_prototype)
    app.add_role("gitlab", link_to_gitlab)
    app.add_role("issue", LinkToGitlabIssue("primary"))
    app.add_role("issues", LinkToGitlabIssues("primary"))

    return {
        "version": "0.0.1",
        "parallel_write_safe": False,
        "parallel_read_safe": False,
    }
