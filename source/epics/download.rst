.. _download:

Download of raw data
====================

:issues:`Download`

We need to implement a method to download the data that is displayed on the
map, such that the user can access and analyze the raw data with their own
scripts. Obviously, the download should then not only be restricted to the
visible data, but should provide the option for multiple layers, times and
parameters.

The idea here is to have three possibilities for the implementation:

1. As we plan for the visualization via WMS (see :issue:`25`), we want to find
   a way how to populate download parameters from a WCS. The selection of
   parameters and times is then transformed into a request to the WCS service
2. describe where (and when) the data can be found on the SFTP-Server
3. generate a download url via a backend module
4. define the size of the data download (data quality in addition to extent and
   resolution) according to user needs
5. Generate a python code block that allows to download the data directly via
   the OpenDAP interface of the underlying THREDDS server and then save it
