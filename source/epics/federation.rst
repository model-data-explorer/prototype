.. _federation:

Federation and Harvesting of Model Data Explorers
=================================================

:issues:`Federation`

The Model Data Explorer should be as FAIR as possible. There are many reasons,
why one would want to run his or her own Model Data Explorer.

One example might be to manage your own THREDDS-Server via the Django-solutions
that we develop here, or to add other Django apps on top for the evaluation of
model data.

Than we should implement the possibility to harvest other instances of the
Model Data Explorer. Within this epic, we want to define

1. how to ensure a safe mapping from model run at Model Data Explorer 1 to
   Model Data Explorer 2
2. how to ensure the correct mapping of relations between model runs, users and
   groups between the two model data explorer instances
