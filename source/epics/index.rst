.. _epics:

Epics in the prototype development
==================================

We use SCRUM project management for the model data explorer. As such, we
divided the tasks into multiple so-called epics. Each epic corresponds to one
topic that is addressed by the model data explorer.

Each epic contains a list of issues that we want to solve (in SCRUM, you'd call
them *User stories*). You'll find all these issues and their discussions in the
gitlab repository and you're kindly invited to contribute your thoughts (see
:ref:`gitlab`).

Each issue has a label that describes the epic. You can use this label to filter
for the issues that corresponds to a specific epic. Or you can use the links
on this website.



.. toctree::

    metadata
    map-frontend
    statistics
    thredds
    download
    sftp
    stakeholders
    federation
