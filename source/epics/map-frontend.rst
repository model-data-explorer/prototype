.. _map-frontend:

Map Frontend
============

:issues:`Map Frontend`

In this epic we want to shape the central element of the model data explorer,
the Map interface. Model runs can define services such as WMS, RestService or
WFS as a visual representation of the model run. This visualization is based on
standardized interfaces for which we want to develop work flows that describe
how the scientists can generate and add them to the model data explorer
themselves.

The outcome of this epic is the prototype UI that defines 

- how to find data in the Model Data Explorer
- and how to select what to visualize

We are also talking in this epic about the work flows how to automatize the
generation of services using the metadata of the OGC-standards. If one has a
working WMS on a THREDDS-Server for instance, it should be straight-forward to
generate the visualization in the model data explorer.

