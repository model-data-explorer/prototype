.. _metadata:

Metadata
========

:issues:`Metadata`

Within this EPIC, we want to clarify the work flows for the handling of
metadata within the Model Data Explorer framework.

Central questions are

1. What is the necessary metadata for model runs?
2. How to populate the metadata of a model run?
3. How to search and filter based on the metadata?
4. What are the possible relations and what do they imply between

   1. groups
   2. users and groups?
   3. model runs?
   4. groups and model runs?
   5. users and model runs?

The outcome of this epic will be a document Metadata handling in the Model Data
Explorer that describes these relations and serves as a basis for the final
documentation and SOPs within the the Model Data Explorer.

.. toctree::
   :maxdepth: 1
   :caption: Metadata user stories

   metadata/terminology
   metadata/relations
   metadata/datasets