.. _dataset-metadata:

Metadata of datasets
====================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`18`
      - :bdg-warning:`In Progress`

.. card:: Section authors:

    .. authorlist::

        Philipp S. Sommer
        Marie Ryan
        Linda Baldewein
        Hatef Takyar
        Andrea Pörsch
        Beate Geyer
        Lars Buntemeyer
        Emanuel Söding
        Nikolaus Groll
        Ludwid Lierhammer
        Klaus Getzlaff
        Tilman Dinter


A central aspect in the model data explorer is the metadata of the datasets.
We do not want to mimic a metadata portal such as geonetwork, but nevertheless,
we need information to

1. let the user know what he or she is looking at
2. link datasets to groups
3. link datasets to people

Each author and group has a dedicated site where all the related datasets are
listed. So we need to find a way to uniquely identify authors and associate the
datasets with them.

We will implement a manual way where you can select the authors and edit the
metadata through a web interface, but it should be possible to automatically
interprete metadata standards.

In the following sections, we will describe what metadata we implement in the,
and how.

.. note::

    This document only covers global metadata of a dataset. Variable related
    metadata (units, standard name, etc.) shall be handled in a different
    document.

    .. todo::

        Document variable related metadata

.. _required-metadata:

Required metadata
-----------------
Datasets in the model data explorer must define the following metadata
attributes:

:ref:`title <md-title>`
    A one-line description of the dataset

.. _optional-metadata:

Optional metadata
-----------------
Optional but recommended metadata attributes are:

:ref:`contacts <md-contacts>`
    A list of authors that have some role related to the dataset. They
    participated in the generation, are responsible for providing the data, etc.
:ref:`institutions <md-institutions>`
    The institutions that are responsible for the dataset
:ref:`projects <md-projects>`
    related projects that provided funding for the generation of the dataset
:ref:`bbox <md-bbox>`
    The bounding box of the geographic region of the data
:ref:`abstract <md-abstract>`
    A short description of the dataset
:ref:`data_relations <md-data-relations>`
    datacite relation types (see :ref:`relations` and
    https://support.datacite.org/docs/relationtype_for_citation)
:ref:`temporal_extent <md-temporal-extent>`
    The temporal window that is covered by the dataset

.. todo::

    document geographic and temporal resolution as well?


.. todo::

    Add descriptive spatial extent (such as global, continental, etc.)


.. todo::

    add creation, publication and revision date


Interpretation of standards
---------------------------
The items mentioned in the previous sections are encoded in the metadata
standards that we support, namely the netCDF header and the INSPIRE ISO-
Standard. Our aim is to develop readers for each standard that transform the
corresponding conventions into the metadata scheme of the model data explorer
(see next section, :ref:`Implementation`).

The exact database structure that allows this interpretation is however part
of a different user story, namely :issue:`20`.

.. _read-netcdf:

CF-Conventions
^^^^^^^^^^^^^^
For netCDF Headers (and NcML, a special markup language used by THREDDS) we
want to develop guidelines based on the
`Binding Regulations for Storing Data as netCDF Files`_. For this purpose, we
will transform the guidelines into a web-based format and enhance it with
templates to make them easier to apply.

The guidelines are based on the CF-Conventions and extend by further attributes
that are mainly motivated by the Conversion methodology to INSPIRE developed
at the Geomar.

.. todo::

    The ``UnidataDD2MI.xsl`` methodology needs to be elaborated further.

.. _Binding Regulations for Storing Data as netCDF Files: https://hereon-netcdf.readthedocs.io


.. _read-iso-inspire:

INSPIRE ISO
^^^^^^^^^^^

ISO-conform XML files will be read using the owslib_ python library. We will
orient the format on the `UnidataDD2MI.xsl` file that has been developed by
Franziska Weng (Geomar) and Andrea Pörsch (GFZ) (currently still work in
progress).

.. _owslib: https://geopython.github.io/OWSLib/

.. _metadata-db:

Implementation
--------------

.. note::

    .. django-source-graph::
        :alt: Illustration of object attributes vs. object relations
        :align: right
        :caption: Object attribute vs. object relations
        :name: graph-attribute-vs-relation

        class Author(models.Model):

            name = models.CharField(max_length=100)
            email = models.EmailField(max_length=100)

        class Dataset(models.Model):

            title = models.CharField(max_length=50)
            abstract = models.CharField(max_length=50, null=True, blank=True)
            contacts = models.ManyToManyField(Author)

    Metadata items described above are represented in the model data explorer
    as properties of Django objects that in turn translates into connections
    and attributes in a relational database. But for this document we will keep
    it simple and distinguish two metadata types: attributes and relations.

    Attributes are simple string properties of a dataset. A `title` for
    instance. Relations describe how the dataset is connected to other items in
    the database. A dataset won't have an `authors` string property, for
    instance, but it will define a connection to author objects, where one
    author holds a first name and last name attribute (for instance).

    An example is shown in the graph on the right, :ref:`graph-attribute-vs-relation`.

.. contents::
    :local:
    :depth: 1

Attributes
^^^^^^^^^^

A `Dataset` defines three simple attributes, `title`, `abstract` and bounding
box (`bbox`), see the graph about :ref:`graph-dataset-attributes`.

.. django-source-graph::
    :align: right
    :caption: Attributes of a dataset
    :name: graph-dataset-attributes

    class Dataset(models.Model):

        title = models.CharField(max_length=50)
        abstract = models.TextField(max_length=10000, null=True, blank=True)
        bbox = models.JSONField(null=True, blank=True)
        start = models.DateTimeField(null=True, blank=True)
        end = models.DateTimeField(null=True, blank=True)

        start_s = models.CharField(max_length=50, null=True, blank=True)
        end_s = models.CharField(max_length=50, null=True, blank=True)

.. _md-title:

Title
"""""

The `title` is a short human-readable description as string of the dataset and should describe
the purpose of the data in one sentence.

.. card:: Interpretation of the title

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            The CF-Conventions define a ``title`` netCDF attribute that will be
            used

        .. tab-item:: INSPIRE
            :sync: inspire

            We are using the ``<gmd:title>`` tag of the ``CI_Citation`` element.

.. _md-abstract:

Abstract
""""""""
The `abstract` is a longer human-readable description of the dataset that
describes the content, purpose and methodology in a bit more details.

.. card:: Interpretation of the abstract

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            We wiill look for global `summary` or `abstract` attribute.

        .. tab-item:: INSPIRE
            :sync: inspire

            We are using the ``<gmd:abstract>`` tag.

.. _md-bbox:

Bounding box
""""""""""""
The `bbox` is a JSONField (or optionally we can also make it a georeferenced
polygon) that defines the region where this dataset can be applied.

.. card:: Interpretation of the bounding Box

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            We wiill look for the global `geospatial_lon_min`,
            `geospatial_lat_min`, `geospatial_lon_max` and `geospatial_lat_max`
            attributes, as well as a `Bbox` attribute.

        .. tab-item:: INSPIRE
            :sync: inspire

            We are using the ``EX_GeographicBoundingBox`` element in the
            ``<gmd:geographicElement>`` tag, namely ``westBoundLongitude``,
            ``eastBoundLongitude``, ``southBoundLatitude`` and
            ``northBoundLatitude``

.. _md-temporal-extent:

Temporal extent
"""""""""""""""
The temporal extent is a ``DatetimeField`` that defines the start and end of a
time window. We will expect two
:abbr:`ISO-formatted timestamps (e.g. 2007-03-13T07:35:10Z)` here, one for the
start and one for the end of the coverage.

This might not always be possible, as python does not support paleo dates. So
we will also add a attributes ``start_s`` and ``end_s`` that accept plain text
fields.

.. card:: Interpretation of the temporal extent

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            We wiill look for the global `StartTime`,
            `StopTime`, `time_coverage_start` and `time_coverage_end`
            attributes.

        .. tab-item:: INSPIRE
            :sync: inspire

            We are using the ``EX_TemporalExtent`` element in the
            ``<gmd:temporalElement>`` tag, namely ``beginPosition``,
            ``endPosition``

.. _md-contacts:

Authors and Contact Persons
^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. todo::

    Add OrcID

.. tab-set::

    .. tab-item:: Description

        Authors can have a dedicated role related when being related to a
        dataset. This roles describe how the authors have been involved in the
        generation of the dataset (motivated by the available roles for
        the ``CI_RoleCode`` tag in INSPIRE, see the :ref:`contact-roles-tab` tab).

        In the metadata display on the frontend, we will then group the
        contributors based on their role such that is clearly visible who is
        the responsible contact person.

    .. tab-item:: Graph

        .. django-source-graph::

            class Author(models.Model):

                name = models.CharField(max_length=100)
                email = models.EmailField(max_length=100)

            class DatasetContact(models.Model):

                author = models.ForeignKey(Author, on_delete=models.CASCADE)
                dataset = models.ForeignKey("Dataset", on_delete=models.CASCADE)
                role = models.CharField(max_length=30)

            class Dataset(models.Model):

                title = models.CharField(max_length=50)
                contacts = models.ManyToManyField(Author, through=DatasetContact)

    .. tab-item:: Roles
        :name: contact-roles-tab

        Roles are taken from the INSPIRE ISO standard,
        https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole.
        The *role* of an `Author` in a `Dataset` can be one of the following:

        .. list-table::
            :header-rows: 1

            * - Code
              - Description
            * - |resourceProvider|
              - Party that supplies the resource.
            * - |custodian|
              - Party that accepts accountability and responsibility for the data and ensures appropriate care and maintenance of the resource.
            * - |owner|_
              - Party that owns the resource.
            * - |user|
              - Party who uses the resource.
            * - |distributor|
              - Party who distributes the resource.
            * - |originator|
              - Party who created the resource
            * - |pointOfContact|
              - Party who can be contacted for acquiring knowledge about or acquisition of the resource.
            * - |principalInvestigator|
              - Key party responsible for gathering information and conducting research.
            * - |processor|
              - Party who has processed the data in a manner such that the resource has been modified.
            * - |publisher|
              - Party who published the resource.
            * - |author|
              - Party who authored the resource.

.. |resourceProvider| replace:: ``resourceProvider``
.. _resourceProvider: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/resourceProvider

.. |custodian| replace:: ``custodian``
.. _custodian: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/custodian

.. |owner| replace:: ``owner``
.. _owner: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/owner

.. |user| replace:: ``user``
.. _user: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/user

.. |distributor| replace:: ``distributor``
.. _distributor: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/distributor

.. |originator| replace:: ``originator``
.. _originator: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/originator

.. |pointOfContact| replace:: ``pointOfContact``
.. _pointOfContact: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/pointOfContact

.. |principalInvestigator| replace:: ``principalInvestigator``
.. _principalInvestigator: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/principalInvestigator

.. |processor| replace:: ``processor``
.. _processor: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/processor

.. |publisher| replace:: ``publisher``
.. _publisher: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/publisher

.. |author| replace:: ``author``
.. _author: https://inspire.ec.europa.eu/metadata-codelist/ResponsiblePartyRole/author


.. card:: Interpretation of Authors and Contact Persons

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            Although the CF-Conventions define an `originator` attribute, the
            information is rather limited. Therefore we aim to follow the
            suggestions by the `UnidataDD2MI.xsl` file of Franziska Weng
            (see :ref:`read-iso-inspire`), and introduce further attributes such
            as `creator_email`, `originator_email`, `contact_email`, `pi_email`,
            `contributor_role`, etc.

        .. tab-item:: INSPIRE
            :sync: inspire

            The implementation is pretty straight-forward and will be taken from
            the `CI_ResponsibleParty` tags.

.. _md-projects:

Projects
^^^^^^^^

.. tab-set::

    .. tab-item:: Description

        Projects are data groups within the Model Data Explorer Framework
        (see :ref:`term-datagroup`). As such, a `project` is also a relation
        between two objects in the database (see the :ref:`projects-graph-tab`
        tab).

        This relation can also be equipped with permissions, namely
        `can_edit`, `can_view` and `can_list` (see
        :ref:`relation-dataset-datagroup`). These permissions need to be
        approved by both, the data group (project) owner and the dataset.

        A relation can also be made visible or invisible, which will determine
        whether the group is listed explicitly on the detail page of the
        dataset or not.

        .. note::

            A dataset can also be related to other types of data groups, such as
            `institutions` and this will be using the same methodology as this.
            As such, we will distinguish projects from platforms, etc. based on the
            ``DS_InitiativeTypeCode`` identifier (see :ref:`term-datagroup` and
            :issue:`20`).

    .. tab-item:: Graph
        :name: projects-graph-tab

        .. django-source-graph::
            :caption: Representation of the Dataset - Project relation.
            :name: graph-dataset-project

            class DataGroup(models.Model):
                name = models.CharField(max_length=100)


            class RelationPermission(models.Model):

                name = models.CharField(max_length=20)
                left_approved = models.BooleanField(default=False)
                right_approved = models.BooleanField(default=False)


            class DatasetDataGroupRelation(models.Model):

                data_group = models.ForeignKey(DataGroup, on_delete=models.CASCADE)
                dataset = models.ForeignKey("Dataset", on_delete=models.CASCADE)
                permissions = models.ManyToManyField(RelationPermission)
                visible = models.BooleanField(default=True)


            class Dataset(models.Model):

                title = models.CharField(max_length=50)
                data_groups = models.ManyToManyField(
                    DataGroup, through=DatasetDataGroupRelation
                )


.. card:: Interpretation of Projects

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            netCDF files can define a `project`, `program`, `projects` or
            `project_name` attribute. We will then search for matching names in
            the data groups that define a ``DS_InitiativeTypeCode`` kind of
            project and suggest them to the data submitter. This will also be
            documented in the netCDF guidelines (see :ref:`read-netcdf`).

        .. tab-item:: INSPIRE
            :sync: inspire

            We will look for ``MD_AggregateInformation`` entries that define
            a ``DS_AssociationTypeCode`` of ``largerWorkCitation`` and match
            the ``MD_Identifier`` against the available data groups.


.. _md-institutions:

Institutions
^^^^^^^^^^^^

.. todo::

    Add ROR ID

Institutions are handled the same internally as :ref:`projects <md-projects>`
as both are represented as :ref:`data groups <term-datagroup>` in the model
data explorer. Just the interpretation of the metadata standards differ.

.. card:: Interpretation of Institutions

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            netCDF files can define an `institution` or `creator_institution`
            attribute, together with a corresponding `institution_references`
            attribute. They will then be matched against available names of
            institutions in the database to make suggestions to the data
            submitter.

        .. tab-item:: INSPIRE
            :sync: inspire

            Institutions will be identified from the ``organisationName`` in a
            ``CI_ResponsibleParty`` (see :ref:`md-contacts` above).

.. _md-data-relations:

Other relations
^^^^^^^^^^^^^^^

.. tab-set::

    .. tab-item:: Description

        Other relations are references to internal or external resources, such as
        related studies or datasets. They are commonly described by datacite related
        identifiers, see https://support.datacite.org/docs/relationtype_for_citation.

        However, neither the CF-Conventions nor INSPIRE define such a relation
        type. But both give the possibilities to add supplementary studies,
        (see below) and we'll just add these informations as a
        `DatasetReference` object (see the :ref:`dataset-references-graph-tab`
        tab).

        If the URI however corresponds to a handle in the model data explorer,
        we can also directly transfer this into a relation between datasets
        (see :ref:`dataset-references-graph-tab` tab) and suggest that
        `Dataset A is supplement to Dataset B` (see :ref:`relation-datasets`).

    .. tab-item:: Graph
        :name: dataset-references-graph-tab

        .. django-source-graph::

            class Dataset(models.Model):

                title = models.CharField(max_length=50)

            class DatasetReference(models.Model):

                dataset = models.ForeignKey(Dataset, on_delete=models.CASCADE)
                description = models.CharField(max_length=400)
                uri = models.URLField(max_length=300)

            class DatasetRelation(models.Model):

                left = models.ForeignKey(Dataset, on_delete=models.CASCADE, related_name="left_relation")
                right = models.ForeignKey(Dataset, on_delete=models.CASCADE, related_name="right_relation")
                relation_type = models.CharField(max_length=30)


.. card:: Interpretation of relations

    .. tab-set::

        .. tab-item:: CF-Conventions
            :sync: CF

            netCDF files can define global `references` and `doi` attributes.
            We will check here for common DOI patterns and use this to extract
            the uri for the ``DatasetReference`` (see the
            :ref:`dataset-references-graph-tab` tab above).

        .. tab-item:: INSPIRE
            :sync: inspire

            INSPIRE encodes references as ``MD_AggregateInformation`` with a
            specific ``DS_AssociationTypeCode``, namely ``crossReference``. So
            we will just use the ``MD_Identifier`` of these tags. If the
            ``MD_Identifier`` is listed as ``gmd:code``, we will assume it's a
            DOI and transform it to the corresponding URL, otherwise we take
            it as the description of the ``DatasetReference`` and try if we
            find a URL in it.
