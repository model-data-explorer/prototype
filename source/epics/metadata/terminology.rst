.. _terminology:

Terminology
===========

.. _term-relation:

Relation
--------

Relations consist of three parts. Let’s look at the following example:

    *coastDat-3_COSMO-CLM_ERAi has been forced with Era Interim*.

The three parts that describe this relation are:

1. The left object (here *coastDat-3_COSMO-CLM-ERAi*)
2. the right object (here *Era Interim*)
3. The description of the relation (here *has been forced with*)

Each relation has an inverse, in this case the inverse is

*Era Interim forces coastDat-3_COSMO-CLM-ERAi*


.. _term-permission:

Permissions
-----------

Sometimes, a *relation* between two items can also be interpreted as a
*permission*. If *CoastDat is created by Hereon*, the dataset owner of
*Coastdat* implicitly grants the *Hereon* group the permission to list this
dataset as one of theirs.

Permissions apply between `datagroups <relation-datagroup>`
:ref:`datasets and datagroups <relation-dataset-datagroup>`, between
:ref:`datasets and users <relation-user-dataset>`,
and between :ref:`users and datagroups <relation-user-datagroup>`.

.. _term-role:

Roles
-----
Roles within the model data explorer are purely considered from the metadata
perspective (see :ref:`md-contacts`) and describes how an
:ref:`author <term-author>` or :ref:`datagroup <term-datagroup>` has been
involved in the creation of a dataset. When interpreting metadata of a dataset,
e.g. by reading the ISO INSPIRE metadata (see :ref:`dataset-metadata`), the
model data might suggest to grant :ref:`permissions <term-permission>` to users
based on the role that the user had in the dataset.

.. todo::

    Link to interpretation of roles and permissions

.. _term-author:

Author
------

.. todo::

    Add Django graphs for author model

An author in the MDE framework is an individual that participated in the
generation of a dataset. Uniquely identified by his or her OrcID. Each
author will also get a unique handle in the Model Data Explorer.

An author can have multiple aliases (e.g. *Philipp Sommer*, *Philipp S.
Sommer*, etc.)

.. note::

    Not all authors will have OrcIDs. To decrease the amount of
    duplicates, we can ask them, and we can ask their co-authors that
    have an *user* account (as researchgate does). This is something that
    still needs to be clarified.

.. _term-user:

User
----

.. todo::

    Add Django graphs for user model

A user is a person with a login for the Model Data Explorer. A user can
be uniquely identified from the email address. Every user is related to
one specific *author*.

.. _term-dataset:

Dataset
-------

.. todo::

    Add Django graphs for dataset model

A **dataset** (formerly known as *model run*) is a collection of
variables with a temporal, spatial and optional vertical dimension. It
can be the output of a single run of your model, or other raster data,
e.g. derived from satellite measurements. A dataset might comprise
multiple parameters. All data that is represented by one single
*dataset* has been generated with one specific methodology and one
specific set of input parameters.

In the language of CERA and CMIP6, one *dataset* in the Model Data
Explorer corresponds to one *experiment*,
e.g. `coastDat-3_COSMO-CLM_ERAi in
CERA <https://cera-www.dkrz.de/WDCC/ui/cerasearch/entry?acronym=coastDat-3_COSMO-CLM_ERAi>`__
or rcp45 in CMIP6.

Each dataset in the model data explorer will have a unique persistent
identifier (PID) and a unique page where it’s possibilities
(visualization on the map, statistical analysis, download, metadata are
accessible).


Examples
~~~~~~~~

One *dataset* in the model data explorer might correspond to - the
output that you generated with one runscript of your model - an ensemble
mean of multiple models and/or experiments - multiple realizations
(ensemble members) produced with variations of the same scenario (in
CMIP6: all data for a given model (e.g. *MPI-ESM-LR*) in a given
experiment (e.g. *rcp85*)) - a single ensemble member with multiple
variables for one CMIP6 scenario of one model with a specific forcing -
data from a specific satellite derived with a specific method

Examples that would **not** be a *dataset* are: - a subset data, e.g. 1
out of 99 years of a model run (unless you want to throw away the other
98 years) - Multiple experiments (e.g. a combination of *rcp26*,
*rcp45*, *rcp60* and *rcp85*) of one model participating in CMIP6
(unless you only refer to ensemble statistics) - Multiple runs of the
same model (unless they are follow-ups of each other, e.g. via restart
files) - Data from multiple models (unless you only refer to the
combined ensemble statistics)

.. _term-datagroup:

Data Group
----------

.. todo::

    Add Django graphs for datagroup model

A **data group** is a group of *datasets* (see above) that are managed
by a certain set of users.

Each *data group* will have an own webpage and a unique handle where all
the datasets are accessible and the metadata of the group is shown.

A *data group* is owned by a certain set of users (see below) and can
have regular members.

Datasets can be associated with groups and provide edit and access
rights.

.. _examples-1:

Examples
~~~~~~~~

-  a research center, e.g. *Hereon*
-  a department or institute, e.g. the `Institute of Coastal Systems -
   Analysis and
   Modeling <https://www.hereon.de/institutes/coastal_systems_analysis_modeling/index.php.en>`__
-  a unit, e.g. `Regional Land and Atmosphere
   Modeling <https://www.hereon.de/institutes/coastal_systems_analysis_modeling/regional_land_atmosphere_modeling/index.php.en>`__
-  a project, e.g. CMIP6, MOSES, or MuSSEL


.. _term-tag:

Tags
----

A standard way to describe a dataset or a data group would be to assign
so-called *tags* or *keywords*. We follow this approach in the model data
explorer. A tag in the model data explorer might have different aliases
(e.g. `coastDat3` and `coastDat-3`), and we implement them as
*hierarchical tags* (e.g. `coastDat > coastDat-3`).

Examples
~~~~~~~~

- `coastDat-3_COSMO-CLM_ERAi` has the `coastDat3` tag
-  the `CMIP6` has a `rcp45` child tag
