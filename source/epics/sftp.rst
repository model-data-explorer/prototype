.. _sftp:

Data upload
===========

:issues:`Data upload`

In order to enable the upload of files for the THREDDS-Server (see :ref:`thredds`),
we want to setup an SFTP-Server.

Users can ask for folders and a quota for a model run, which is then granted by
the admins. This folder is tight to the user groups in the shared
LDAP-authentication to configure the read-/write access from the Django
application.

Within this EPIC we want to evaluate SFTP vs. the webDAV protocol and MAMS (as
suggested by the IT). If we decide to go for SFTP, we want to continue with
defining and demonstrating the work flows to

- request for space on the data volume
- tie this folder to the permission system
- link the folders with related users, groups and model runs
