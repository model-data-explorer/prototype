.. _statistical-analysis:

Statistical analysis
====================

:issues:`Statistical analysis`

The model data explorer will be based upon the
`Digital Earth Messaging Framework`_, a remote procedure call framework for
distributed data analysis. We want to offer the user the possibility to analyze
and compute on the data without downloading it, through the web and through
scripts (e.g. Python).

Within this epic, we want to discuss the implementation of this distributed
analysis work flow. How it can be configured, secured and designed for a
maximum of flexibility and usability.

We want to generate work flow descriptions and documentations about:

- the basic API for a backend module
- how to create a backend module from templates
- how to register a backend module
- how to secure the backend module
- how to visualize the features of a backend module in the Model Data Explorer
  Web UI
- how to access the features of a backend module from script

.. _Digital Earth Messaging Framework: https://sorse.github.io/programme/talks/event-048/