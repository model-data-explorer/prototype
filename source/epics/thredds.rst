.. _thredds:

THREDDS Server configuration and implementation
===============================================

:issues:`THREDDS`

THREDDS server are very rich in functionalities and provides many services that
can be used within the model data explorer. And it integrates well with the
data upload functionalities of the SFTP server (see :ref:`sftp`).

However, the standard setup has some disadvantages:

1.	the catalog entries can only be edited by admins of the THREDDS-server
2.	catalogues are written with and XML syntax that is unknown to most scientists
3.	the handling and overview of multiple catalogue files can be quite a challenge
4.	one cannot give access to all catalogue files to individual scientists in order to prevent unauthorized access

This is why we intend to develop a django app that generates the catalogue
entries using the power of the Django templating system. Another advantage of
using Django is that we can integrate it with the permission and relation
system of the Model Data Explorer (see :issue:`21` and :issue:`17`). This
management app lets the scientists configure their own content on the THREDDS
and how it is displayed.

Within this epic, we want to learn more about the functionalities of the
THREDDS-Server through discussions with more experienced THREDDS-Server-Managers,
and we want to define the necessary features and workflows that need to be made
available. In the end, we want an implementation in the prototype to demonstrate
these workflows.
We will, however, for now only schedule one user story to discuss the features
of the THREDDS-Server. Within this user story, we define how we want to proceed.
