.. _faqs:

Frequently Asked Questions
==========================

Here we collect questions related to the different aspects on the model data
explorer.

Relations
---------

.. dropdown:: Models are usually forced by multiple static
    (=initial) and temporally varying boundary conditions (=forcings).
    How can *right* or *left* be *lists*?

    From :ref:`relation terminology <term-relation>`

    You do not use a list, but one dataset can define the
    same relation type to multiple other datasets. E.g.

    -  coastDat-3_COSMO-CLM_ERAi has been forced with Era Interim
    -  coastDat-3_COSMO-CLM_ERAi has been forced with some other dataset


.. dropdown:: The inverse relationship does not make sense for
    boundary conditions, as typically a boundary conditions (say:
    bathymetry) has inifinite possibilities of forcing models …

    From :ref:`relation terminology <term-relation>`

    Relations between datasets are optional. A dataset does
    not have to define a relation about the forcing, etc. Equally, one
    dataset might serve as forcing for an multiple datasets.


Datasets
--------

.. dropdown:: How do you deal with unconvential (modular)
    modeling systems, such as MOSSCO, where the number of things
    *left* and *right* are on the order of 10-20? Theoretical example:
    ``MuSSeL_SCHISM-physics-SED3D-sediment-ECOSMO-ecosystem-filtration-ICEALGAE-WW3-waves-CICE-ice-forced-by-EHYP-rivers-NOAH-porosity-MPIOM-ESM-LR-Atmosphere-RCP8.5-HYCOM-ecosystem-boundaries-TOPX-tides-IOW-bathymetrie-version-30045-windfarm-parameterization-2035``
    Seems rather impractical.

    From :ref:`dataset terminology <term-dataset>`

    We want to keep the name of a dataset informative. On WDCC, we often
    include the forcing etc. in the name, we do not want to rely on this in the
    Model Data Explorer.
