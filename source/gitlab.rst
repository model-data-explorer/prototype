.. _gitlab:

Usage and navigation on Gitlab
==============================

During the prototype development, we will make heavy use of the issues on
Gitlab.

The model data explorer has a dedicated group at
https://gitlab.hzdr.de/model-data-explorer and the prototype development takes
place in the :gitlab:`prototype repository on gitlab`.

We created several issues there that we want to discuss during this first phase
before we start with the development.

Email Notifications
-------------------
The project is public, but as a member of the project team, you are
automatically made a member of this repository. Note that you might want to set
the notifications to `Participate`, otherwise you will get a mail for each and
every comment on this repository. You can edit your notification settings with
the bell symbol in the upper right corner of the :gitlab:`repository website`.

If you are then particularly interested in a specific issue, you should turn on
the notifications and you'll be notified via mail when there is any progress.
This can be achieved with the *Notifications* switch in the sidebar on the
right of the detail view of an issue (see :issue:`5` for instance).

Navigation
----------
Every issue on this repository is also assigned to an epic (see :ref:`epics`),
indicated by the purple issue label. You can click on that label to filter for
all issues of this epic. As an alternative, every epic in this documentation
also has a link to the corresponding issues, see :ref:`metadata` for instance.

Participation
-------------
Please feel free to comment to the individual issues on gitlab or create new
ones with questions or topics you want to discuss. If you never worked with
gitlab, it's pretty simple. Just go to the detail page of the issue :issue:`5`
for instance, and enter your comment in the text field at the bottom of the
website.