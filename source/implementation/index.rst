.. _implementation:

Implementation details
======================

In this part, we want to become a bit more concrete on the exact implementation
in the model data explorer. This is therefore getting a bit more technical.

We describe here the basic implementation with Django and the workflows.

.. toctree::

    core_models
    plugins
    federation
    workflows/index
