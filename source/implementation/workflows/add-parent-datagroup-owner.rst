.. _workflow-add-parent-datagroup-owner:

Add an owner for a parent ``DataGroup`` workflow
================================================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

When a new owner (or any other role) is added to a ``DataGroup``, we need to
make sure that this owner is also added to the ``DataGroupUserGroup``\ s of
the ``DataGroup``\ s children. Here we can use the ``DataGroupNode`` in the
`parent` graph to query the children and add the new user to the corresponding
user groups (see :ref:`fig-add-parent-datagroup-owner` for an illustration).

.. _fig-add-parent-datagroup-owner:

.. figure:: /_static/diagrams/add-parent-datagroup-owner.png
    :alt: Add Parent DataGroup Owner

    Sequence Diagram to add a new owner to a parent ``DataGroup``
