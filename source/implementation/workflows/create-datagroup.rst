.. _workflow-create-datagroup:

Create DataGroup workflow
=========================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`21`
      - :bdg-warning:`In Progress`


Every registered user in the model data explorer can create a ``DataGroup`` and
become an owner it. Once a ``DataGroup`` object has been created, several
``DataGroupUserGroup`` objects are created, one for each role that a user can
have in a datagroup (see :ref:`relation-user-datagroup` and
:ref:`implementation-datagroup-relations`). Furthermore, we create one
``DataGroupNode`` per graph type, see :ref:`relation-datagroups` and
:ref:`datagroup-permission-graphs`.

The user who created the ``DataGroup`` is then added as an owner for this
data group and automatically added to the corresponding ``DataGroupUserGroup``.

This workflow with ``DataGroupUserGroup``\ s provides an efficient workflow as
permissions to datasets, etc. are granted to the user group and not to the
individual owner. As such, if a user does stops being an owner, data editor,
etc. we just remove him or her from the corresponding ``DataGroupUserGroup``\ s
and he or she looses all the permissions.

A more schematic illustration of this workflow is displayed in the figure
:ref:`fig-create-datagroup`.


.. _fig-create-datagroup:

.. figure:: /_static/diagrams/create-datagroup.png
    :alt: Create DataGroup

    Sequence Diagram to create a ``DataGroup``
