.. _workflow-create-dataset:

Create Dataset workflow
=======================


.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`21`
      - :bdg-warning:`In Progress`


Every registered user in the model data explorer can generate a ``Dataset``.
This then automatically creates a ``DatasetUserRelation`` that highlights him
or her as an owner of the dataset.

See :ref:`implementation-dataset-relations` for a more detailed explanation
of the models, and :ref:`fig-create-dataset` for a schematic illustration of
the workflow.

.. _fig-create-dataset:

.. figure:: /_static/diagrams/create-dataset.png
    :alt: Create Dataset

    Sequence Diagram to create a ``Dataset``
