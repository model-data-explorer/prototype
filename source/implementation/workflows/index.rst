.. _implementation-workflows:

Workflow descriptions
=====================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`


Here we describe the different workflows in the model data explorer on a more
technical basis.

Our relation model includes several permission levels and roles as decribed in
:ref:`relation-dataset-datagroup` and :ref:`relation-datagroups`. In this
section, we describe how the permissions are implemented and what happens if we
change relations between users, datagroups and datasets.

We document this with so-called `sequence diagrams`. If you are not
familiar with this concept of diagrams, please read the following short
description.

.. dropdown:: Explanation on Sequence Diagrams

    Sequence diagrams, as their name suggests, describe a sequence of
    actions between actors (such as a `DataGroup Owner`) and objects (such
    as a `DataGroup`).

    Each actor and owner has a so-called `lifeline`. If action `A` is below
    action `B` on a `lifeline`, then `A` happens before `B`.

    An `action` in that sense is visualized by an arrow. A solid arrow
    means that the object/actor requests something, a dashed arrow means
    that the object/actor returns something.

    Furthermore we have so-called interaction fragments, denoted by a
    gray rectangle. Here we use the following fragments:

    `loop`
        A `loop` fragment implies a loop on the arguments that
        are given in square brackets (e.g. ``[DataGroupNode]``)
    `ref`
        A `ref` fragment (also called `interaction use`) points to a
        different sequence diagram (e.g. the `Link DataGroups` fragment
        refers to :ref:`fig-link-datagroups`).
    `alt`
        An `alt` fragment refers to alternative decisions, like an
        `if-else`-case. We usually use these to document something like
        *if approved by DataGroup Owner, do something, else, revert*.

    If you want to know more about sequence diagrams, please have a look
    at https://www.uml-diagrams.org/sequence-diagrams.html.

.. toctree::
    :caption: DataGroup workflows

    create-datagroup
    link-datagroups
    refresh-datagroupusergroup
    add-parent-datagroup-owner
    remove-parent-datagroup-owner
    unlink-datagroups


.. toctree::
    :caption: Dataset workflows

    create-dataset
    link-dataset-datagroup
