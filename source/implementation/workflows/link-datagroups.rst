.. _workflow-link-datagroups:

Link ``DataGroups`` workflow
----------------------------

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

Each owner of a ``DataGroup`` can link the group to another one. He or she can
then select whether the group should be a child or a parent of the other group.
In any case, an owner of the other group has to confirm the membership.

The figure :ref:`fig-link-datagroups` illustrates this workflow for the case
that the owner of the `child` ``DataGroup`` requests the link. A
``DataGroupRelation`` object is created (see also
:ref:`implementation-datagroup-relations`) and the owners of the other
(`parent`) ``DataGroup`` get an email with a link to approve or reject the
request. If rejected, the ``DataGroupRelation`` object is deleted again,
otherwise the users of ``DataGroupUserGroup``\ s of the `parent` ``DataGroup``
are added to the corresponding ``DataGroupUserGroup``\ s of the child.

In the future now, all users from the parents ``DataGroupUserGroup``\ s are
automatically added or removed from the childs ``DataGroupUserGroup``\ s
(see :ref:`workflow-add-parent-datagroup-owner` and
:ref:`workflow-add-parent-datagroup-owner`).

.. _fig-link-datagroups:

.. figure:: /_static/diagrams/link-datagroups.png
    :alt: Link DataGroups Sequence

    Sequence Diagram to link two ``DataGroup``\ s in the `parent` graph