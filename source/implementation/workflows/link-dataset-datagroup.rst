.. _workflow-link-dataset-datagroup:

Link ``Dataset`` and ``DataGroup`` workflow
-------------------------------------------

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

An owner of a dataset or and owner of a datagroup can request to link the two
objects. He or she can then assign roles for this relation as described in
:ref:`relation-dataset-datagroup`.

The figure :ref:`fig-link-dataset-datagroup` illustrates this workflow for the
case that the dataset creator links the dataset to another datagroup.

Once such a link is requested, a ``DatasetDataGroupRelation`` object is created
and the owners of the ``DataGroup`` are asked to approve the relation. If they
approve, the corresponding permissions on the ``Dataset`` are granted to the
``DataGroupUserGroup`` of the ``DataGroup``. If the ``DataGroup`` owner
rejects, the ``DatasetDataGroupRelation`` is deleted.

As all owners of parent ``DataGroup``\ s are contained in the
``DataGroupUserGroup`` (see :ref:`workflow-link-datagroups`), they also get the relevant
permissions.

.. _fig-link-dataset-datagroup:

.. figure:: /_static/diagrams/link-dataset-datagroup.png
    :alt: Link Dataset and DataGroup

    Sequence Diagram to link a ``Dataset`` to a ``DataGroup``
