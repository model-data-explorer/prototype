.. _workflow-refresh-datagroupusergroup:

Refresh a ``DataGroupUserGroup`` workflow
-----------------------------------------

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

There are multiple scenario, where the ``DataGroupUserGroup`` of a data group
needs to be updated. E.g. when a user has been removed from the list of owners
of the data group itself, or from one of it's parent ``DataGroup``\ s (see
:ref:`workflow-remove-parent-datagroup-owner` for instance).

To efficiently refresh the owners of the data group, we need to query the
``DataGroupUserGroup``\ s of all it's parents and add the resulting users to
the ``DataGroupUserGroup`` of the ``DataGroup`` that is about to be refreshed.

Using the ``DataGroupNode`` relations implemented with django-postgresql-dag_,
we can get all owners of the parents with one single database transaction and
synchronize the ``DataGroupUserGroup`` with this list.

The figure :ref:`fig-refresh-datagroupusergroup` illustrates this workflow.

.. _django-postgresql-dag: https://github.com/OmenApps/django-postgresql-dag

.. _fig-refresh-datagroupusergroup:

.. figure:: /_static/diagrams/refresh-datagroupusergroup.png
    :alt: Refresh the DataGroupUserGroup

    Sequence Diagram to refresh the owners of a ``DataGroup``