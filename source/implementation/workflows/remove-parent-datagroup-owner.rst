.. _workflow-remove-parent-datagroup-owner:

Remove an owner from a parent ``DataGroup`` workflow
=====================================================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

Removing an owner (or any other role) from a ``DataGroup`` is a bit more tricky
than adding one. As in the workflow to
:ref:`add a new owner <workflow-add-parent-datagroup-owner>`, we use the
``DataGroupNode`` of the `parent` graph to query the children. However, now we
cannot simply remove the user from the corresponding ``DataGroup`` as we need
to check whether their might be any other parent ``DataGroup`` of the child
that justifies the owners role for the child ``DataGroup``. So we query the
parents users or we
:ref:`refresh the data group <workflow-refresh-datagroupusergroup>`.

This workflow is illustrated in :ref:`fig-remove-parent-datagroup-owner`.


.. _fig-remove-parent-datagroup-owner:

.. figure:: /_static/diagrams/remove-parent-datagroup-owner.png
    :alt: Remove Parent DataGroup Owner

    Sequence Diagram to remove an owner from a parent ``DataGroup``
