.. _workflow-unlink-datagroups:

Unlink DataGroups workflow
==========================

.. list-table::
    :header-rows: 1

    * - Related Issues
      - Document Status
    * - :issue:`17`
      - :bdg-warning:`In Progress`
    * - :issue:`21`
      - :bdg-warning:`In Progress`

Unlinking two ``DataGroup``\ s (i.e. removing the relation that has been added
with :ref:`workflow-link-datagroups`) is the same as
:ref:`removing a user from a parent DataGroup <workflow-remove-parent-datagroup-owner>`
and is illustrated in :ref:`fig-unlink-datagroups`. We need to query all child
``DataGroupNode``\ s and trigger a
:ref:`refresh of the user groups <workflow-refresh-datagroupusergroup>`.

Afterwards, we remove the ``DataGroupRelation`` object.

.. _fig-unlink-datagroups:

.. figure:: /_static/diagrams/unlink-datagroups.png
    :alt: Unlink DataGroups

    Sequence Diagram to remove the link between two ``DataGroup``\ s