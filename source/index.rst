.. _landing-page:

Welcome to the Prototype development of the Model Data Explorer!
================================================================

This document describes the first phase of the development of the model data
explorer. We try to be as inclusive as possible so please let us know if you
encounter any issues or have any questions on the process.

Within the `prototype` development, we discuss the workflows and develop a
prototype to get a feeling how the model data explorer will work.

The development team comes up with suggested workflows and discusses this with
the scientific collaborators.

.. only:: html

      .. dropdown:: Contributors

         .. authorlist::
               :all:

Resources
---------
We have three important resources for during this phase of development:

1. The prototype, see :ref:`prototype`.
2. The Epics described in this document, see :ref:`epics`.
3. The issues in the gitlab repository, see :ref:`gitlab`.

Contact
-------
Please do not hesitate to contact the development team directly if you have any
questions or points for discussion. We are reacheable via

- comments in the :prototype:`prototype`
- the Model Data Explorer `Mattermost channel`_,
- the mde-dev_ mailing list
- or via email at hcdc_support@hereon.de.

.. _mde-dev: https://www.listserv.dfn.de/sympa/subscribe/mde-dev
.. _Mattermost channel: https://mattermost.hzdr.de/ak-datenanalyse/channels/model-data-explorer
.. _Gitlab: https://gitlab.hzdr.de/model-data-explorer

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: Resources

   Main documentation <https://model-data-explorer.readthedocs.io>

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: Documentation

   prototype
   epics/index
   implementation/index
   gitlab
   faqs

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Development

   Development workspace <https://model-data-explorer.readthedocs.io/projects/workspace>
   todo



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
