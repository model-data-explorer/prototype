.. _prototype:

Using the prototype
===================

You can access the prototype at |figma_prototype|. This prototype is
interactive but it's just a preview on how the model data explorer might look
and feel. Everything you see that is not really automated.

Here are some hints on the usage of this prototype:

- Click anywhere on the prototype and you'll see the clickable elements
  highlighted in blue. We recomment to use these elements for navigation rather
  than the slide show buttons in the bottom center.
- We are happy to receive your comments in the prototype. For this, use the
  little speech bubble in the upper left corner. Please note that you have to
  register to leave comments (it's free).
- The prototype is not made for mobile devices. But the model data explorer
  will be designed mobile responsive.
- Unless you are experienced with Figma, we recommend that you stay in the
  prototype mode and to not click on `Open in editor` (which is what you could
  do from the dropdown menu in the upper center).
